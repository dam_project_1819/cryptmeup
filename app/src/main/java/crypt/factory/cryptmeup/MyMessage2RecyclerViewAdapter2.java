package crypt.factory.cryptmeup;

import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import crypt.factory.cryptmeup.Classes.DownloadImage;
import crypt.factory.cryptmeup.Classes.Message;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyMessage2RecyclerViewAdapter2 extends RecyclerView.Adapter<MyMessage2RecyclerViewAdapter2.ViewHolder> {

    private final List<Message> OutboxList;
    private final OutboxFragment.OnListFragmentInteractionListener mListener;

    public MyMessage2RecyclerViewAdapter2(List<Message> messages, OutboxFragment.OnListFragmentInteractionListener listener) {
        OutboxList = messages;
        mListener = listener;
    }

    @Override
    public MyMessage2RecyclerViewAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_message2, parent, false);
        return new MyMessage2RecyclerViewAdapter2.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyMessage2RecyclerViewAdapter2.ViewHolder holder, int position) {
        holder.mItem = OutboxList.get(position);
        holder.LabelMessage.setBackgroundColor(Color.WHITE);

        try {
            Bitmap img = null;
            if (OutboxList.get(position).getImgBitmap() == null){
                img = new DownloadImage().execute(OutboxList.get(position).getMsg_ImageUrl()).get();
                holder.ImgPreview.setImageBitmap(img);
                OutboxList.get(position).setImgBitmap(img);
            }
            else {
                img = OutboxList.get(position).getImgBitmap();
                holder.ImgPreview.setImageBitmap(img);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        holder.Receiver.setText(OutboxList.get(position).getMsg_Receiver());
        holder.idImagen.setText(OutboxList.get(position).getMsg_IdImage());

        String year = String.valueOf(OutboxList.get(position).getMsg_DateSend().getYear());
        String month = String.valueOf(OutboxList.get(position).getMsg_DateSend().getMonth());
        String day = String.valueOf(OutboxList.get(position).getMsg_DateSend().getDayOfMonth());
        String LongFormat = day+"-"+month+"-"+year;

        String hour = String.valueOf(OutboxList.get(position).getMsg_DateSend().getHour());
        String minute = String.valueOf(OutboxList.get(position).getMsg_DateSend().getMinute());
        String ShortFormat = hour+":"+minute;

        int minutesAgo = LocalDateTime.now().getMinute() - OutboxList.get(position).getMsg_DateSend().getMinute();
        minute = String.valueOf(minutesAgo);
        String MinuteAgo = minute+"m ago";

        int secondsAgo = LocalDateTime.now().getSecond() - OutboxList.get(position).getMsg_DateSend().getSecond();
        String seconds = String.valueOf(secondsAgo);
        String NowFormat = seconds+"s ago";

        Boolean todayBool = LocalDateTime.now().getDayOfYear() > OutboxList.get(position).getMsg_DateSend().getDayOfYear();
        Boolean lastHour = LocalDateTime.now().getHour() > OutboxList.get(position).getMsg_DateSend().getHour();
        Boolean lastMinut = LocalDateTime.now().getMinute() > OutboxList.get(position).getMsg_DateSend().getMinute();
        if (todayBool) {
            holder.TimeMessage.setText(LongFormat);
        }
        else if (lastHour) {
            holder.TimeMessage.setText(ShortFormat);
        }
        else if (lastMinut) {
            holder.TimeMessage.setText(MinuteAgo);
        }
        else {
            holder.TimeMessage.setText(NowFormat);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return OutboxList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView Receiver;
        public final TextView idImagen;
        public final TextView TimeMessage;
        public final ImageView ImgPreview;
        public final LinearLayout LabelMessage;
        public Message mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            Receiver = (TextView) view.findViewById(R.id.Receiver);
            idImagen = (TextView) view.findViewById(R.id.idImagen);
            TimeMessage = (TextView) view.findViewById(R.id.TimeMessage);
            ImgPreview = (ImageView) view.findViewById(R.id.ImgPreview);
            LabelMessage = (LinearLayout) view.findViewById(R.id.LabelMessage);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + Receiver.getText() + "'" + TimeMessage.getText() + "'" + idImagen.getText() + "'";
        }
    }
}
