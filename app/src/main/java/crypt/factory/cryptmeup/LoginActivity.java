package crypt.factory.cryptmeup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import crypt.factory.cryptmeup.Classes.credencials;
import crypt.factory.cryptmeup.Classes.usuari;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login) Button _loginButton;
    @BindView(R.id.link_signup) TextView _signupLink;
    SharedPreferences sf;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        comprovarLogin();

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }

    private void comprovarLogin() {
        sf = PreferenceManager.getDefaultSharedPreferences(this);
        boolean registered = sf.getBoolean("registered", false);
        if(registered){
            onLoginSuccess();
        }
    }

    public boolean comprobarCredenciales(String usr, String passwd){

        boolean login = false;
        usuari User = new usuari();
        try {
            User = new credencials(usr,passwd).execute("hola").get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (User.getLogin()){
            login = true;

            SharedPreferences.Editor editor = sf.edit();
            editor.putBoolean("registered",true);
            editor.putString("user_id",String.valueOf(User.getId_usuari()));
            editor.putString("user_CPub",User.getCPublica());
            editor.apply();
        }

        return login;
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        String email = "";
        if (_emailText.getText().toString().contains("@")){
            email = _emailText.getText().toString().split("@")[0];
        }
        else {
            email = _emailText.getText().toString();
        }
        String password = _passwordText.getText().toString();

        boolean login = comprobarCredenciales(_emailText.getText().toString(),password);
        if (login) {

            sf = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
            SharedPreferences.Editor editor = sf.edit();
            editor.putBoolean("registered",true);
            editor.putString("usr",email);
            editor.putString("usrEmail",_emailText.getText().toString());
            editor.apply();

            onLoginSuccess();
        }
        else {
            _emailText.requestFocus();
            _emailText.setError("Invalid Email");

            _passwordText.requestFocus();
            _passwordText.setError("Invalid Password");
            _passwordText.setText("");
            onLoginFailed();
            progressDialog.dismiss();
            return;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // SQL insert into usuaris
                // crea patró QR

                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty()) { // || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()  si solament email afegir aixo
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 30) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
}

