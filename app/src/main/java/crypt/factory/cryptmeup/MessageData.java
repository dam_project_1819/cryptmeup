package crypt.factory.cryptmeup;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Base64.Encoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;

import javax.crypto.Cipher;

import crypt.factory.cryptmeup.Classes.Message;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MessageData.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MessageData#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageData extends Fragment {

    private static final String ARG_PARAM1 = "missatge";
    private Message message;

    private OnFragmentInteractionListener mListener;
    private TextView DecryptedData;
    private SharedPreferences sf;

    public MessageData() {

    }

    // TODO: Rename and change types and number of parameters
    public static MessageData newInstance(Message missatge) {
        MessageData fragment = new MessageData();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, (Serializable) missatge);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = (Message) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View Inflate = inflater.inflate(R.layout.fragment_message_data, container, false);
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.hide();

        sf = PreferenceManager.getDefaultSharedPreferences(getActivity());

        ImageView imgView = (ImageView) Inflate.findViewById(R.id.imgView);
        imgView.setImageBitmap(message.getImgBitmap());
        message.setMsg_Delivered(true);
        message.setMsg_Downloaded(LocalDateTime.now());
        DecryptedData = (TextView) Inflate.findViewById(R.id.textResultDecrypted);

        Button button = (Button) Inflate.findViewById(R.id.buttonDecrypt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View Inflate) {
                String Data = DecryptDataImage();
                DecryptedData.setText(Data);
            }
        });

        return Inflate;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private String DecryptDataImage() {
        String result = null;

        PrivateKey privKey = null;
        try {
            ObjectInputStream objectInputStream =
                    new ObjectInputStream(new FileInputStream(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"keys-"+sf.getString("user_id","")+".txt"));

            privKey = (PrivateKey) objectInputStream.readObject();

            objectInputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String M = "";

        try {
            byte[] missatgeX = readMissatgeFromFile(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"missatge.txt");
            Cipher rsaCipher = Cipher.getInstance("RSA");
            rsaCipher.init(Cipher.DECRYPT_MODE, privKey);
            M = new String(rsaCipher.doFinal(missatgeX), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Bitmap Imatge = message.getImgBitmap();

        result = "Message Unencrypted: "+M;

        return result;
    }

    private static byte[] readMissatgeFromFile(String url) {
        byte[] resultat = null;
        try {
            File file = new File(url);
            resultat = new byte[(int) file.length()];
            InputStream is = new FileInputStream(file);
            is.read(resultat);
            is.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultat;
    }
}
