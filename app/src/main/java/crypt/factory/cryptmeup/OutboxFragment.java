package crypt.factory.cryptmeup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import crypt.factory.cryptmeup.Classes.Message;
import crypt.factory.cryptmeup.Classes.Messages;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class OutboxFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private SharedPreferences sf;
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OutboxFragment.OnListFragmentInteractionListener mListener;

    private List<Message> OutboxList = new ArrayList<Message>();
    private boolean FirstLoaded = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private Messages messages;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OutboxFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static OutboxFragment newInstance(int columnCount) {
        OutboxFragment fragment = new OutboxFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list2, container, false);

        sf = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Outbox - "+sf.getString("usr","Crypt Me Up"));

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.show();

        recyclerView = view.findViewById(R.id.list);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (!FirstLoaded) {
            FirstLoaded = true;
            readDataFromXML();
        }

        fillOutbox();

        return view;
    }

    private void fillOutbox() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());

        // Set the adapter
        if (recyclerView instanceof RecyclerView) {
            Context context = recyclerView.getContext();
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(mLinearLayoutManager);
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyMessage2RecyclerViewAdapter2(this.OutboxList, mListener));

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), mLinearLayoutManager.getOrientation());
            recyclerView.addItemDecoration(dividerItemDecoration);
        }
    }

    private void readDataFromXML() {

        OutboxList.clear();
        messages = null;

        try {
            String fileName = sf.getString("fileEnviatsURL",null);

            InputStream input = null;

            try {
                input = new FileInputStream(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Serializer ser = new Persister();

            try {
                this.messages = ser.read(Messages.class, input);
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (messages != null && messages.getMessageList() != null) {
                for (Message M : messages.getMessageList()) {
                    M.setMsg_DateSend(LocalDateTime.parse(M.getAux_msg_DateSend()));
                    M.setMsg_Delivered(Boolean.parseBoolean(M.getAux_msg_Delivered()));
                    M.setMsg_Downloaded(LocalDateTime.parse(M.getAux_msg_Downloaded()));
                    M.setMsg_Hash(M.getAux_msg_Hash().getBytes());
                    M.setMsg_ImageUrl("https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg");
                    OutboxList.add(0,M);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        /*byte[] HASH = new byte[20];
        new Random().nextBytes(HASH);

        Message M = new Message("code 001", "jo", "tu", LocalDateTime.now(),
                "id imagen", "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg", HASH,
                null, false);
        Message M2 = new Message("code 001", "jo", "tu", LocalDateTime.now(),
                "id imagen", "https://www.w3schools.com/w3css/img_lights.jpg", HASH,
                null, false);
        OutboxList.add(M);
        OutboxList.add(M2);*/

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OutboxFragment.OnListFragmentInteractionListener) {
            mListener = (OutboxFragment.OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        readDataFromXML();
        fillOutbox();
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Message item);
    }
}
