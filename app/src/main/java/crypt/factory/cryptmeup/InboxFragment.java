package crypt.factory.cryptmeup;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import crypt.factory.cryptmeup.Classes.Message;
import crypt.factory.cryptmeup.Classes.Messages;
import crypt.factory.cryptmeup.Classes.getInboxFromFTP;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class InboxFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    private List<Message> InboxList = new ArrayList<Message>();
    private boolean FirstLoaded = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private SharedPreferences sf;
    private Messages messages;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public InboxFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static InboxFragment newInstance(int columnCount) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list, container, false);
        sf = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Inbox - "+sf.getString("usr","Crypt Me Up"));

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.show();

        recyclerView = view.findViewById(R.id.list);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (!FirstLoaded) {
            FirstLoaded = true;
            readDataFromXML();
        }

        fillInbox();

        return view;
    }

    private void fillInbox() {
        mLinearLayoutManager = new LinearLayoutManager(getContext());

        // Set the adapter
        if (recyclerView instanceof RecyclerView) {
            Context context = recyclerView.getContext();
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(mLinearLayoutManager);
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyMessageRecyclerViewAdapter(this.InboxList, mListener));

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), mLinearLayoutManager.getOrientation());
            recyclerView.addItemDecoration(dividerItemDecoration);
        }
    }

    private void readDataFromXML() {

        InboxList.clear();

        try {
            InboxList = new getInboxFromFTP(sf.getString("user_id","")).execute("hola").get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*byte[] HASH = new byte[20];
        new Random().nextBytes(HASH);

        Message M = new Message("code 001", "jo", "tu", LocalDateTime.now().toString(),
                "id imagen", "https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg", new String(HASH),
                null, "false");
        Message M2 = new Message("code 001", "jo", "tu", LocalDateTime.now().toString(),
                "id imagen", "https://www.w3schools.com/w3css/img_lights.jpg", new String(HASH),
                null, "false");
        Message M3 = new Message("code 001", "jo", "tu", LocalDateTime.now().toString(),
                "id imagen", "https://www.w3schools.com/w3css/img_lights.jpg", new String(HASH),
                null, "false");
        Message M4 = new Message("code 001", "jo", "tu", LocalDateTime.now().toString(),
                "id imagen", "https://www.w3schools.com/w3css/img_lights.jpg", new String(HASH),
                null, "false");
        InboxList.add(M);
        InboxList.add(M2);
        InboxList.add(M3);
        InboxList.add(M4);*/

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        readDataFromXML();
        fillInbox();
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Message item);
    }
}
