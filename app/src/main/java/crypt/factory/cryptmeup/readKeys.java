package crypt.factory.cryptmeup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class readKeys extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private TextView resultTextView;
    private QRCodeReaderView qrCodeReaderView;
    private SharedPreferences sf;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_keys);

        sf = PreferenceManager.getDefaultSharedPreferences(readKeys.this);

        askPermissions();

        resultTextView = (TextView) findViewById(R.id.resultQR);

        qrCodeReaderView = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();
    }

    private void askPermissions() {
        //--------------------PERMISOS---------------------//
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.CAMERA)) {
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        //-------------------------------------------------//
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        String Key = text;
        WriteToStorage(text);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    public void WriteToStorage(String keys){

        InputStream in = null;
        OutputStream os = null;

        try {
            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"keys-"+sf.getString("user_id","")+".xml";
            File outFile = new File(filePath);

            PreferenceManager.getDefaultSharedPreferences(readKeys.this);
            SharedPreferences.Editor editor = sf.edit();
            editor.putString("fileKeys",outFile.toString());
            editor.apply();

            if (!outFile.exists()) {

                outFile.getParentFile().mkdirs();
                outFile.createNewFile();
                os = new FileOutputStream(outFile);

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);
                outputStreamWriter.write(keys);
                outputStreamWriter.close();

                os.flush();
                os.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
