package crypt.factory.cryptmeup;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import crypt.factory.cryptmeup.Classes.Message;

public class AddMessage extends Fragment {

    private SharedPreferences sf;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;

    private OnFragmentInteractionListener mListener;
    private String sender = null;
    private Message message;
    private String destFile;
    EditText txtText;

    public AddMessage() {
        // Required empty public constructor
    }

    public static AddMessage newInstance() {
        AddMessage fragment = new AddMessage();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View Inflate =  inflater.inflate(R.layout.fragment_add_message, container, false);

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.hide();

        sf = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Write New Message");
        message = new Message();

        TextView txtFrom = (TextView) Inflate.findViewById(R.id.messageFrom);
        sender = sf.getString("usr","---");
        txtFrom.setText(sender);

        final EditText txtTo = (EditText) Inflate.findViewById(R.id.messageTo);

        txtText = (EditText) Inflate.findViewById(R.id.messageText);

        Button btnBrowse = (Button) Inflate.findViewById(R.id.btnBrowse);
        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View Inflate) {

                //--------------------PERMISOS---------------------//
                if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    }

                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                //-------------------------------------------------//

                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 25);

            }
        });

        Button btnEcrypt = (Button) Inflate.findViewById(R.id.btnEncrypt);
        btnEcrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View Inflate) {

                byte[] array = new byte[30];
                new Random().nextBytes(array);
                int idnum = new Random().nextInt(99999999);
                String msgCode = new String(array, Charset.forName("UTF-8"));
                String idImatge = "#"+idnum;

                //penjar url

                if (message.getImgBitmap() == null) {
                    AssetManager assetManager = getContext().getAssets();

                    InputStream istr;
                    Bitmap bitmap = null;
                    try {
                        istr = assetManager.open("image.png");
                        bitmap = BitmapFactory.decodeStream(istr);
                        int alt = (int) (bitmap.getHeight() * 1080 / bitmap.getWidth());
                        Bitmap reduit = Bitmap.createScaledBitmap(bitmap, 1080, alt, true);
                        message.setImgBitmap(reduit);
                    } catch (IOException e) {}
                }

                PublicKey pubKey = null;
                try {
                    ObjectInputStream objectInputStream =
                            new ObjectInputStream(new FileInputStream(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"keys2-"+sf.getString("user_id","")+".txt"));

                    pubKey = (PublicKey) objectInputStream.readObject();

                    objectInputStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                try {
                    Cipher rsaCipher = Cipher.getInstance("RSA");
                    rsaCipher.init(Cipher.ENCRYPT_MODE, pubKey);
                    String mensaje = txtText.getText().toString();
                    byte[] mensajeCifrado = rsaCipher.doFinal(mensaje.getBytes(StandardCharsets.UTF_8));
                    escriure(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"missatge.txt", mensajeCifrado);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                }

                message.setMsg_Code(msgCode);
                message.setMsg_Receiver(txtTo.getText().toString());
                message.setMsg_Sender(sender);
                message.setAux_msg_DateSend(LocalDateTime.now().toString());
                message.setMsg_IdImage(idImatge);
                message.setMsg_ImageUrl("https://i.imgur.com/hVmph8E.jpg");
                message.setAux_msg_Hash(Arrays.toString(hashBitmap(message.getImgBitmap())));
                message.setAux_msg_Downloaded(LocalDateTime.now().toString());
                message.setAux_msg_Delivered("false");

                writeXML(message);

                //------CLOSE------//
                changeFragment(OutboxFragment.class);
            }
        });

        return Inflate;
    }

    public byte[] hashBitmap(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bitmapBytes = baos.toByteArray();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {}
        byte[] hash = md.digest(bitmapBytes);
        return hash;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 25 && resultCode == Activity.RESULT_OK && null != intent){
            Uri seleccio = intent.getData();
            String[] columna = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(seleccio, columna, null, null, null);
            cursor.moveToFirst();
            int indexColumna = cursor.getColumnIndex(columna[0]);
            String rutaFitxer = cursor.getString(indexColumna);
            cursor.close();
            Bitmap imatge = BitmapFactory.decodeFile(rutaFitxer);
            int alt = (int) (imatge.getHeight() * 1080 / imatge.getWidth());
            Bitmap reduit = Bitmap.createScaledBitmap(imatge, 1080, alt, true);

            message.setImgBitmap(reduit);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void writeXML(Message message) {

        String destFile = sf.getString("fileEnviatsURL",null);

        try {
            if (destFile != null) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setValidating(false);
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(new FileInputStream(new File(destFile)));

                Element root = doc.getDocumentElement();

                Element newMessage = doc.createElement("missatge");

                Element codiMissatge = doc.createElement("codiMissatge");
                codiMissatge.appendChild(doc.createTextNode(message.getMsg_Code()));
                newMessage.appendChild(codiMissatge);

                Element destinatari = doc.createElement("destinatari");
                destinatari.appendChild(doc.createTextNode(message.getMsg_Receiver()));
                newMessage.appendChild(destinatari);

                Element emisor = doc.createElement("emisor");
                emisor.appendChild(doc.createTextNode(message.getMsg_Sender()));
                newMessage.appendChild(emisor);

                Element data_enviar = doc.createElement("data_enviar");
                data_enviar.appendChild(doc.createTextNode(message.getAux_msg_DateSend()));
                newMessage.appendChild(data_enviar);

                Element id_imatge = doc.createElement("id_imatge");
                id_imatge.appendChild(doc.createTextNode(message.getMsg_IdImage()));
                newMessage.appendChild(id_imatge);

                Element url = doc.createElement("url");
                url.appendChild(doc.createTextNode(message.getMsg_ImageUrl()));
                newMessage.appendChild(url);

                Element hash = doc.createElement("hash");
                hash.appendChild(doc.createTextNode(message.getAux_msg_Hash()));
                newMessage.appendChild(hash);

                Element data_descarga = doc.createElement("data_descarga");
                data_descarga.appendChild(doc.createTextNode(message.getAux_msg_DateSend()));
                newMessage.appendChild(data_descarga);

                Element obert = doc.createElement("obert");
                obert.appendChild(doc.createTextNode(message.getAux_msg_Delivered()));
                newMessage.appendChild(obert);

                root.appendChild(newMessage);

                DOMSource source = new DOMSource(doc);

                TransformerFactory tFactory = TransformerFactory.newInstance();
                Transformer transformer = tFactory.newTransformer();
                StreamResult result = new StreamResult(destFile);
                transformer.transform(source,result);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeFragment(Class fragmentClass){
        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null){
            FragmentTransaction ft = manager.beginTransaction();
            ft.remove(AddMessage.this);
            ft.replace(R.id.content_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public static void escriure (String nomfitxer, byte[] bytes) {
        OutputStream os = null;

        try {
            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"missatge.txt";
            File outFile = new File(filePath);

            outFile.getParentFile().mkdirs();
            outFile.createNewFile();
            os = new FileOutputStream(outFile);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(os);
            os.write(bytes);
            outputStreamWriter.close();

            os.flush();
            os.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
