package crypt.factory.cryptmeup.Classes;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "missatges")
public class Messages {

    @ElementList(required = true, inline = true)
    private ArrayList<Message> MessageList;

    public Messages() {
        MessageList = new ArrayList<Message>();
    }

    public ArrayList<Message> getMessageList() {
        return MessageList;
    }

    public void setMessageList(ArrayList<Message> messageList) {
        MessageList = messageList;
    }
}
