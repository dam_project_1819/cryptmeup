package crypt.factory.cryptmeup.Classes;

import android.graphics.Bitmap;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

@Root(name = "missatge")
public class Message implements Serializable {
    @Element(name = "codiMissatge")
    private String msg_Code;

    @Element(name = "destinatari")
    private String msg_Receiver;

    @Element(name = "emisor")
    private String msg_Sender;

    @Element(name = "data_enviar")
    private String aux_msg_DateSend;

    private LocalDateTime msg_DateSend;

    @Element(name = "id_imatge")
    private String msg_IdImage;

    @Element(name = "url")
    private String msg_ImageUrl;

    @Element(name = "hash")
    private String aux_msg_Hash;

    private byte[] msg_Hash;

    @Element(name = "data_descarga")
    private String aux_msg_Downloaded;

    private LocalDateTime msg_Downloaded;

    @Element(name = "obert")
    private String aux_msg_Delivered;

    private Boolean msg_Delivered;

    private Bitmap imgBitmap;

    public Message() {

    }

    public Message(String msg_Code, String msg_Receiver, String msg_Sender, String aux_msg_DateSend,
                   String msg_IdImage, String msg_ImageUrl, String aux_msg_Hash, String aux_msg_Downloaded,
                   String aux_msg_Delivered) {
        this.msg_Code = msg_Code;
        this.msg_Receiver = msg_Receiver;
        this.msg_Sender = msg_Sender;
        this.aux_msg_DateSend = aux_msg_DateSend;
        setMsg_DateSend(LocalDateTime.parse(aux_msg_DateSend));
        this.msg_IdImage = msg_IdImage;
        this.msg_ImageUrl = msg_ImageUrl;
        this.aux_msg_Hash = aux_msg_Hash;
        setMsg_Hash(aux_msg_Hash.getBytes());
        if (aux_msg_Downloaded == null){
            this.aux_msg_Downloaded = null;
            setMsg_Downloaded(null);
        }
        else {
            this.aux_msg_Downloaded = aux_msg_Downloaded;
            setMsg_Downloaded(LocalDateTime.parse(aux_msg_Downloaded));
        }
        this.aux_msg_Delivered = aux_msg_Delivered;
        setMsg_Delivered(Boolean.parseBoolean(aux_msg_Delivered));
    }

    public String getMsg_Code() {
        return msg_Code;
    }

    public String getMsg_Receiver() {
        return msg_Receiver;
    }

    public String getMsg_Sender() {
        return msg_Sender;
    }

    public LocalDateTime getMsg_DateSend() {
        return msg_DateSend;
    }

    public String getMsg_IdImage() {
        return msg_IdImage;
    }

    public String getMsg_ImageUrl() {
        return msg_ImageUrl;
    }

    public byte[] getMsg_Hash() {
        return msg_Hash;
    }

    public LocalDateTime getMsg_Downloaded() {
        return msg_Downloaded;
    }

    public Boolean getMsg_Delivered() {
        return msg_Delivered;
    }

    public Bitmap getImgBitmap() {
        return imgBitmap;
    }

    public void setMsg_Code(String msg_Code) {
        this.msg_Code = msg_Code;
    }

    public void setMsg_Receiver(String msg_Receiver) {
        this.msg_Receiver = msg_Receiver;
    }

    public void setMsg_Sender(String msg_Sender) {
        this.msg_Sender = msg_Sender;
    }

    public void setMsg_DateSend(LocalDateTime msg_dateSend){
        this.msg_DateSend = msg_dateSend;
    }

    public void setMsg_IdImage(String msg_IdImage) {
        this.msg_IdImage = msg_IdImage;
    }

    public void setMsg_ImageUrl(String msg_ImageUrl) {
        this.msg_ImageUrl = msg_ImageUrl;
    }

    public void setMsg_Hash(byte[] msg_Hash) {
        this.msg_Hash = msg_Hash;
    }

    public void setImgBitmap(Bitmap imgBitmap) {
        this.imgBitmap = imgBitmap;
    }

    public void setMsg_Downloaded(LocalDateTime msg_Downloaded) {
        this.msg_Downloaded = msg_Downloaded;
    }

    public void setMsg_Delivered(Boolean msg_Delivered) {
        this.msg_Delivered = msg_Delivered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(getMsg_Code(), message.getMsg_Code()) &&
                Objects.equals(getMsg_Receiver(), message.getMsg_Receiver()) &&
                Objects.equals(getMsg_Sender(), message.getMsg_Sender()) &&
                Objects.equals(getMsg_DateSend(), message.getMsg_DateSend()) &&
                Objects.equals(getMsg_IdImage(), message.getMsg_IdImage()) &&
                Objects.equals(getMsg_ImageUrl(), message.getMsg_ImageUrl()) &&
                Arrays.equals(getMsg_Hash(), message.getMsg_Hash()) &&
                Objects.equals(getMsg_Downloaded(), message.getMsg_Downloaded()) &&
                Objects.equals(getMsg_Delivered(), message.getMsg_Delivered());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMsg_Code(), getMsg_Receiver(), getMsg_Sender(), getMsg_DateSend(), getMsg_IdImage(), getMsg_ImageUrl(), getMsg_Downloaded(), getMsg_Delivered());
        result = 31 * result + Arrays.hashCode(getMsg_Hash());
        return result;
    }

    /*----------------*/

    public void setAux_msg_DateSend(String aux_msg_DateSend) {
        this.aux_msg_DateSend = aux_msg_DateSend;
        LocalDateTime LDT = LocalDateTime.parse(aux_msg_DateSend);
        setMsg_DateSend(LDT);
    }

    public void setAux_msg_Hash(String aux_msg_Hash) {
        this.aux_msg_Hash = aux_msg_Hash;
        byte[] by = aux_msg_Hash.getBytes();
        setMsg_Hash(by);
    }

    public void setAux_msg_Downloaded(String aux_msg_Downloaded) {
        this.aux_msg_Downloaded = aux_msg_Downloaded;
        LocalDateTime LDT = null;
        if (aux_msg_Downloaded != null) {
            LDT = LocalDateTime.parse(aux_msg_Downloaded);
        }
        setMsg_Downloaded(LDT);
    }

    public void setAux_msg_Delivered(String aux_msg_Delivered) {
        this.aux_msg_Delivered = aux_msg_Delivered;
        Boolean bl = Boolean.parseBoolean(aux_msg_Delivered);
        setMsg_Delivered(bl);
    }

    public String getAux_msg_DateSend() {
        return aux_msg_DateSend;
    }

    public String getAux_msg_Hash() {
        return aux_msg_Hash;
    }

    public String getAux_msg_Downloaded() {
        return aux_msg_Downloaded;
    }

    public String getAux_msg_Delivered() {
        return aux_msg_Delivered;
    }
    /*----------------*/
}
