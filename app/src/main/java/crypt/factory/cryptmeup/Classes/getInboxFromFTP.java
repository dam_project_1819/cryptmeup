package crypt.factory.cryptmeup.Classes;

import android.os.AsyncTask;
import android.os.Environment;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class getInboxFromFTP extends AsyncTask<String, Void, List<Message>> {

    private String id_usuari;
    private Messages messages;
    private List<Message> InboxList = new ArrayList<Message>();

    public getInboxFromFTP(String id_usuari) {
        this.id_usuari = id_usuari;
    }

    @Override
    protected List<Message> doInBackground(String... strings) {

        String p_sURL = "https://projectedamcryptmeup.000webhostapp.com/xml/"+id_usuari+".xml";
        URL oURL;
        URLConnection oConnection;
        InputStream in = null;
        OutputStream os = null;
        BufferedReader oReader;

        try
        {
            oURL = new URL(p_sURL);
            oConnection = oURL.openConnection();
            in = oConnection.getInputStream();

            String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"Inbox_"+id_usuari+".xml";
            File outFile = new File(filePath);

            outFile.getParentFile().mkdirs();
            outFile.createNewFile();
            os = new FileOutputStream(outFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1){
                os.write(buffer, 0, read);
            }

            in.close();
            os.close();
            os=null;

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        messages = null;

        try {
            String fileName = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator+"cryptMeUp"+File.separator+"Inbox_"+id_usuari+".xml";

            InputStream input = null;

            try {
                input = new FileInputStream(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Serializer ser = new Persister();

            try {
                this.messages = ser.read(Messages.class, input);
                input.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (messages != null && messages.getMessageList() != null) {
                for (Message M : messages.getMessageList()) {
                    M.setAux_msg_DateSend(M.getAux_msg_DateSend().replace(" ","T"));
                    M.setMsg_DateSend(LocalDateTime.parse(M.getAux_msg_DateSend()));
                    M.setMsg_Delivered(Boolean.parseBoolean(M.getAux_msg_Delivered()));
                    M.setAux_msg_Downloaded(M.getAux_msg_Downloaded().replace(" ","T"));
                    M.setMsg_Downloaded(LocalDateTime.parse(M.getAux_msg_Downloaded()));
                    M.setMsg_Hash(M.getAux_msg_Hash().getBytes());
                    InboxList.add(0,M);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return InboxList;
    }
}
