package crypt.factory.cryptmeup.Classes;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class credencials extends AsyncTask<String, Void, usuari> {

    private String user;
    private String Passwd;

    public credencials(String user, String Passwd) {
        this.user = user;
        this.Passwd = Passwd;
    }

    @Override
    protected usuari doInBackground(String... strings) {

        String p_sURL = "https://projectedamcryptmeup.000webhostapp.com/xml/usuari.xml";
        usuari User = new usuari();
        URL oURL;
        URLConnection oConnection;
        BufferedReader oReader;
        String sLine;
        StringBuilder sbResponse;
        String sResponse = null;

        try
        {
            oURL = new URL(p_sURL);
            oConnection = oURL.openConnection();
            oReader = new BufferedReader(new InputStreamReader(oConnection.getInputStream()));
            sbResponse = new StringBuilder();

            while((sLine = oReader.readLine()) != null)
            {
                sbResponse.append(sLine);
            }

            sResponse = sbResponse.toString();
            System.out.println(sResponse);
            String[] dades = sResponse.split("</usuari>");

            for (String a : dades){
                if (a.contains("<email>"+user+"</email>") && a.contains("<contrasenya>"+Passwd+"</contrasenya>")){


                    User.setId_usuari(Integer.valueOf(a.substring(a.indexOf("<id_usuari>")+11, a.indexOf("</id_usuari>"))));
                    User.setNom(a.substring(a.indexOf("<nom>")+5,a.indexOf("</nom>")));
                    User.setEmail(a.substring(a.indexOf("<email>")+7,a.indexOf("</email>")));
                    User.setCPublica(a.substring(a.indexOf("<CPublica>")+10,a.indexOf("</CPublica>")));
                    User.setContrassenya(a.substring(a.indexOf("<contrasenya>")+13,a.indexOf("</contrasenya>")));
                    User.setLogin(true);

                    break;
                }
                else {
                    User.setLogin(false);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return User;
    }
}
