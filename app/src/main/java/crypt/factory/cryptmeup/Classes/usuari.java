package crypt.factory.cryptmeup.Classes;

public class usuari {

    private int id_usuari;
    private String nom;
    private String email;
    private String CPublica;
    private String Contrassenya;
    private Boolean Login;

    public usuari() {
    }

    public int getId_usuari() {
        return id_usuari;
    }

    public void setId_usuari(int id_usuari) {
        this.id_usuari = id_usuari;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCPublica() {
        return CPublica;
    }

    public void setCPublica(String CPublica) {
        this.CPublica = CPublica;
    }

    public String getContrassenya() {
        return Contrassenya;
    }

    public void setContrassenya(String contrassenya) {
        Contrassenya = contrassenya;
    }

    public Boolean getLogin() {
        return Login;
    }

    public void setLogin(Boolean login) {
        Login = login;
    }
}
